package interfaz;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

import modelos.Estacion;
import mundo.Administrador;


public class Main 
{	
	/**
	 * Administrador del SITP
	 */
	private Administrador admin;

	/**
	 * Lector de la consola
	 */
	private BufferedReader in;

	/**
	 * Inicializa la clase principal de la aplicación
	 */
	public Main() throws Exception {
		admin = new Administrador();
		in = new BufferedReader(new InputStreamReader(System.in));
		System.out.println("<< Bienvenido al sistema integrado de transporte de Bogota >>");
	}

	/**
	 * Menu principal de la aplicacion
	 * @throws Exception
	 */
	public void menuInicial() throws Exception
	{
		String mensaje = "Por favor seleccione  "
				+ "una opcion: \n - - - - - - - - - - - - - - - - - - - - -\n"
				+ "[1] Cargar informacion sistema \n"
				+ "[2] Buscar estación por nombre \n"
				+ "[3] Buscar estación por id \n"
				+ "[4] Rutas que pasan por una estación \n"
				+ "[5] Distancia entre estaciones más cercanas \n"
				+ "[6] Distancia entre estaciones más lejanos \n"
				+ "[7] Salir \n";
		System.out.print(mensaje);
		String op1 = in.readLine();
		
		System.out.println(">> "+ op1);
		switch(op1)
		{
		case "1":
			try
			{
				admin.cargarInformacion();
			}
			catch(Exception e)
			{
				e.printStackTrace();
				System.out.println(e instanceof IOException ? 
						"Ocurrio un error al leer los archivos. por favor intenta de nuevo"
						:"No fue posible extraer la información. Verifica el formato de los archivos");
			}
			break;
		case "2":
			System.out.println(">Ingrese el nombre del estacion");
			Estacion p = admin.buscarParadero(in.readLine().trim()); 
			System.out.println(p!=null ? "La estación buscada es " + p: "estacion no encontrada");
			break;
	    case "3": 
			System.out.println(">Ingrese el identificador del estacion (el id es el mismo nombre de la estacion, solo que en lugar de la letra se pone su equivalente en ASCII --> ejemplo: A=65)");
			try{
				Estacion p1 = admin.darParadero(Integer.parseInt(in.readLine()));
				System.out.println(p1!=null ? p1: "estacion no encontrado");
			}
			catch(Exception e)
			{
				System.out.println("El identificador ingresado no es valido.");
			}
			break;
		case "4": 
			System.out.println(">Ingrese el nombre del estacion");
			String paradero = in.readLine();
			String[] rutas =  admin.darRutasEstacion(paradero);
			System.out.println("Por el paradero "+ paradero+" pasan "+ rutas.length+" ruta(s): ");
			for (int i = 0; i < rutas.length; i++) {
				System.out.println((i+1)+". "+rutas[i]);

			}
			break;
		case "5": 
			double distanciaMenor = admin.distanciaMinimaParaderos();
			if(distanciaMenor != 1000000000)
			System.out.println("Las estaciones más cercanas están a "+distanciaMenor+" metros de distancia");
			else
				System.out.println("Ocurrió un error, intente de nuevo");
			break;
		case "6": 
			double distanciaMayor = admin.distanciaMaximaParaderos();
			if(distanciaMayor != 0)
			System.out.println("Las estaciones más lejanos están a "+distanciaMayor+" metros de distancia");
			else
				System.out.println("ocurrió un error, intente de nuevo");
			break;
		case "7": System.exit(1);
		}
		System.out.println("<<");
	}
	
	public static void main(String[] args) throws Exception {
		Main main = new Main();
		for(;;)
		{
			main.menuInicial();
			System.out.println("Intro para volver al menú inicial");
			main.in.readLine();
		}
	}
}
