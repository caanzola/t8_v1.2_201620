package estructuras;

public class TablaHashLinearProbing<K ,V> 
{
	private NodoHash<Integer, NodoHash<K, V>> tuplasLlaveValor[];

	/**
	 * Tamaño del arreglo fijo. 
	 */
	private int capacidad;


	public TablaHashLinearProbing(int size)
	{
		capacidad = size;
		tuplasLlaveValor = new NodoHash[capacidad];

		for(int i = 0; i < tuplasLlaveValor.length; i++)
		{
			tuplasLlaveValor[i] = new NodoHash<Integer, NodoHash<K,V>>(0, null);
			tuplasLlaveValor[i].setLlave(i); 
		}
	}

	public void put(K llave, V valor)
	{
		int key = hash(llave);
		NodoHash<K, V> nuevoRegistro = new NodoHash(llave, valor); 
		tuplasLlaveValor[key].setValor( nuevoRegistro);	
	}



	public V get(K llave)
	{
		int key = hash(llave);
		V devolver = null;
		if(tuplasLlaveValor[key] != null)
		{
			if(tuplasLlaveValor[key].getValor() != null)
			
				if(	tuplasLlaveValor[key].getValor().getLlave().equals(llave))
			{
			   devolver = (V) tuplasLlaveValor[key].getValor();
			}
		}
		return devolver;
	}

	public V delete(K llave)
	{
		int key = hash(llave);
		V valorDevolver = null;
		if(tuplasLlaveValor[key].getValor() != null)
		{
			if(tuplasLlaveValor[key].getValor().getLlave() == llave)
			{
				valorDevolver = (V) tuplasLlaveValor[key].getValor().getValor();
			}
		}
		
		if(valorDevolver != null)
		tuplasLlaveValor[key].setValor(null);
		
		return valorDevolver;
	}

	private int hash(K pllave)
	{
		int llave = (Integer) pllave.hashCode();
		if(llave <0)
			llave = -llave;
		int key = llave%tuplasLlaveValor.length;
		while(tuplasLlaveValor[key].getValor() != null && tuplasLlaveValor[key].getLlave() != key)
		{
			if(key+2 < tuplasLlaveValor.length)
			{
				key = key+2;
			}
			else
			{
				key = (key+2) - tuplasLlaveValor.length;
			}
		}
		return key;
	}

}
