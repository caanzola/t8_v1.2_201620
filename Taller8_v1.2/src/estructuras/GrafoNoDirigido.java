package estructuras;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import modelos.Estacion;

/**
 * Clase que representa un grafo con peso no dirigido.
 * @author SamuelSalazar
 * @param <T> El tipo que se va a utilizar como nodos del grafo
 */
public class GrafoNoDirigido<T extends Nodo> implements Grafo<T> 
{

	/**
	 * Nodos del grafo
	 */
	//TODO Declare la estructura que va a contener los nodos
	private T nodos[];
	

	/**
	 * Lista de adyacencia 
	 */
	private TablaHashLinearProbing<Integer, ListaHash<Arco>> adj;
	//TODO Utilice sus propias estructuras (defina una representacion para el grafo)
		// Es libre de implementarlo con la representacion de su agrado. 

	private int cantidadDeVectores;
	
	private int ultimo;
	
	/**
	 * Construye un grafo no dirigido vacio.
	 */
	public GrafoNoDirigido() 
	{
		//TODO implementar
		cantidadDeVectores = 0;
		if(cantidadDeVectores != 0)
			inicializarEstructuras();
	}
	
	public void inicializarEstructuras()
	{
		adj = new TablaHashLinearProbing<>(cantidadDeVectores);
		nodos = (T[]) new Estacion[cantidadDeVectores];
		ultimo = 0;
	}

	@Override
	public boolean agregarNodo(T nodo) {
		//TODO implementar
		boolean agrego = false;
		nodos[ultimo] = nodo;
		agrego = true;
		ultimo++;
		return agrego;
	}

	@Override
	public boolean eliminarNodo(int id) {
		//TODO implementar
		return true;
	}

	@Override
	public Arco[] darArcos() {
		//TODO implementar
		Arco[] totalConexiones = new Arco[9759];
		int pointer = 0;
		for (int i = 0; i < nodos.length; i++) 
		{
			
			Estacion estActual = (Estacion) nodos[i];
			if(estActual.darAdyacentes() != null)
			{
				for (int j = 0; j < estActual.darAdyacentes().length; j++) 
				{
					totalConexiones[pointer] = estActual.darAdyacentes()[j];
					pointer++;
				}
			}
		}
		return totalConexiones;
	}

	private <E extends Comparable<E>> Arco crearArco( final int inicio, final int fin, final double costo, E e )
	{
		return new Arco(buscarNodo(inicio), buscarNodo(fin), costo, e);
	}

	@Override
	public Nodo[] darNodos() {
		//TODO implementar
		return nodos;
	}

	@Override
	public <E extends Comparable<E>> boolean agregarArco(int i, int f, double costo, E obj) {
		//TODO implementar
		return true;
	}

	@Override
	public boolean agregarArco(int i, int f, double costo) {
		return agregarArco(i, f, costo, null);
	}

	@Override
	public Arco eliminarArco(int inicio, int fin) {
		//TODO implementar
		return null;
	}

	@Override
	public Nodo buscarNodo(int id) {
		//TODO implementar
		Nodo buscado = null;
		boolean encontro = false;
		if(nodos != null)
		{
			for (int i = 0; i < nodos.length && !encontro; i++) 
			{
				if((Integer)nodos[i].darId() == id)
				{
					buscado = nodos[i];
					encontro = true;
				}
			}
		}
		return buscado;
	}

	@Override
	public Arco[] darArcosOrigen(int id) {
		//TODO implementar
		return null;
	}

	@Override
	public Arco[] darArcosDestino(int id) {
		//TODO implementar
		return null;
	}

	public void guardarTamanioVectores(int tamanio) 
	{
		cantidadDeVectores = tamanio;
		inicializarEstructuras();
	}

}
