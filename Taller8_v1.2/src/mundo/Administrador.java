package mundo;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileReader;
import java.util.Set;
import java.util.TreeSet;

import modelos.Estacion;
import modelos.Ruta;
import estructuras.Arco;
import estructuras.Grafo;
import estructuras.GrafoNoDirigido;
import estructuras.Nodo;

/**
 * Clase que representa el administrador del sistema integrado de transporte
 * @author SamuelSalazar
 *
 */
public class Administrador {

	/**
	 * Ruta del archivo de estaciones
	 */
	public static final String RUTA_PARADEROS ="./data/stations.txt";
	
	/**
	 * Ruta del archivo de rutas
	 */
	public static final String RUTA_RUTAS ="./data/routes.txt";

	
	/**
	 * Grafo que modela el sistema integrado de transporte
	 */
	//TODO Declare el grafo que va a modela el sistema
	private GrafoNoDirigido<Estacion> grafo;
	
	private Ruta[] rutas;

	private int dondeVa;
	
	/**
	 * Construye un nuevo administrador del SITP
	 */
	public Administrador() 
	{
		//TODO inicialice el grafo como un GrafoNoDirigido
		grafo = new GrafoNoDirigido<>(); 
	}

	/**
	 * Devuelve todas las rutas que pasan por un paadero con nombre dado
	 * @param nombre 
	 * @return Arreglo con los nombres de las rutas que pasan por el estacion requerido
	 */
	public String[] darRutasEstacion(String nombre)
	{
		//TODO Implementar
		//Buscar en la lista de rutas y ver si ahi esta esa estacion nombre
		String[] listaRutas = new String[185];
		int pointer = 0;
		if(rutas != null)
		{
			for (int i = 0; i < rutas.length; i++) 
			{
				Ruta rutaActual = rutas[i];
				if(rutaActual != null)
				{
					if(rutaActual.darParadas() != null)
					{
						boolean encontroEstacion = false;
						for (int j = 0; j < rutaActual.darParadas().length && !encontroEstacion; j++) 
						{
							Estacion estacionActual = rutaActual.darParadas()[j];
							if(estacionActual.darNombre().equals(nombre))
							{
								listaRutas[pointer] = rutaActual.darNombre();
								pointer++;
								encontroEstacion = true;
							}
						}
					}
				}
			}
		}
		
		String actual = listaRutas[0];
		int tamanio = 0;
		while(actual != null)
		{
			tamanio++;
			actual = listaRutas[tamanio];
		}
		
		String[] rutasDevolver = new String[tamanio];
		
		for (int i = 0; i < rutasDevolver.length; i++) 
		{
			rutasDevolver[i] = listaRutas[i];
		}
		
		return rutasDevolver;
	}

	/**
	 * Devuelve la distancia que hay entre los paraderos mas cercanos del sistema.
	 * @return distancia minima entre 2 paraderos
	 */
	public double distanciaMinimaParaderos()
	{
		//TODO Implementar
		double distanciaMenor = 1000000000;
		
		if(grafo != null)
		{
			Estacion[] estaciones = (Estacion[]) grafo.darNodos();
			if(estaciones != null)
			{
				for (int i = 0; i < estaciones.length; i++) 
				{
					Estacion estacionActual = estaciones[i];
					Arco[] caminos = estacionActual.darAdyacentes();
					if(caminos != null)
					{
						for (int j = 0; j < caminos.length; j++) 
						{
							Arco caminoActual = caminos[j];
							if(caminoActual != null)
							{
								double distanciaActual = caminoActual.darCosto();
								if(distanciaActual < distanciaMenor)
								{
									distanciaMenor = distanciaActual;
								}
							}
						}
					}
				}
			}
		}
		
		return distanciaMenor;
	}
	
	
	public Estacion darParadero(int id)
	{
		//TODO Implementar
		Estacion respuesta = (Estacion) grafo.buscarNodo(id);
		return respuesta;
	}
	/**
	 * Devuelve la distancia que hay entre los paraderos más lejanos del sistema.
	 * @return distancia maxima entre 2 paraderos
	 */
	public double distanciaMaximaParaderos()
	{
		//TODO Implementar
		double distanciaMayor = 0;
		
		if(grafo != null)
		{
			Estacion[] estaciones = (Estacion[]) grafo.darNodos();
			if(estaciones != null)
			{
				for (int i = 0; i < estaciones.length; i++) 
				{
					Estacion estacionActual = estaciones[i];
					Arco[] caminos = estacionActual.darAdyacentes();
					if(caminos != null)
					{
						for (int j = 0; j < caminos.length; j++) 
						{
							Arco caminoActual = caminos[j];
							if(caminoActual != null)
							{
								double distanciaActual = caminoActual.darCosto();
								if(distanciaActual > distanciaMayor)
								{
									distanciaMayor = distanciaActual;
								}
							}
						}
					}
				}
			}
		}
		
		return distanciaMayor;
	}


	/**
	 * Metodo encargado de extraer la informacion de los archivos y llenar el grafo 
	 * @throws Exception si ocurren problemas con la lectura de los archivos o si los archivos no cumplen con el formato especificado
	 */
	public void cargarInformacion() throws Exception
	{
		//TODO Implementar
		File archivo = new File(RUTA_PARADEROS);
		FileReader fr = new FileReader (archivo); 
		BufferedReader br = new BufferedReader(fr);
		
		int tamanio = Integer.parseInt(br.readLine());
		grafo.guardarTamanioVectores(tamanio); //Se guarda el tamaño de la tabla hash que contendrá la lista de adyacencia
		
		//llenar el grafo
		String linea = br.readLine();
		while(linea != null)
		{
			String nombreEstacion = linea.split(";")[0];
			char letra = nombreEstacion.charAt(3);
			int numChar = letra;
			int id = Integer.parseInt(nombreEstacion.substring(0,3)+ numChar+nombreEstacion.substring(4,6));

			double latitud = Double.parseDouble(linea.split(";")[1]);
			double longitud = Double.parseDouble(linea.split(";")[2]);
			Estacion nuevaEstacion = new Estacion(id, latitud, longitud, nombreEstacion);
			grafo.agregarNodo(nuevaEstacion);
			id++;
			linea = br.readLine();
		}
		
		fr.close();
		br.close();
		
		
		System.out.println("Se han cargado correctamente "+grafo.darNodos().length+" Estaciones");
		
		//TODO Implementar
		
		File archivoRutas = new File(RUTA_RUTAS);
		FileReader frRutas = new FileReader (archivoRutas); 
		BufferedReader brRutas = new BufferedReader(frRutas);
		int tamanioRutas = Integer.parseInt(brRutas.readLine());
		rutas = new Ruta[tamanioRutas];
		dondeVa = 0;
		brRutas.readLine();
		String nombreRuta = brRutas.readLine();
		while(nombreRuta != null)
		{
			int cantidadDeArcos = Integer.parseInt(brRutas.readLine());
			if(cantidadDeArcos != 0)
			{
				Ruta nuevaRuta = new Ruta(nombreRuta, cantidadDeArcos);
				rutas[dondeVa] = nuevaRuta;
				dondeVa++;
				String nombreEstacionInicio = brRutas.readLine();
				char letra = nombreEstacionInicio.charAt(3);
				int numLetra = letra;
				int id = Integer.parseInt(nombreEstacionInicio.substring(0,3) + numLetra + nombreEstacionInicio.substring(4,6));
				Estacion estacionIcicio = (Estacion) grafo.buscarNodo(id);
				nuevaRuta.agregarEstacion(estacionIcicio);

				String nombreEstacionActual = brRutas.readLine();
				estacionIcicio.inicializarArregloDeConexiones(cantidadDeArcos);
				while(nombreEstacionActual != null && !nombreEstacionActual.startsWith("---"))
				{
					double costo = Double.parseDouble(nombreEstacionActual.split(" ")[1]);
					char letra2 = nombreEstacionActual.charAt(3);
					int numLetra2 = letra2;
					int id2 = Integer.parseInt(nombreEstacionActual.substring(0,3) + numLetra2 + nombreEstacionActual.substring(4,6));
					Estacion estacionFinal = (Estacion) grafo.buscarNodo(id2);
					nuevaRuta.agregarEstacion(estacionFinal);
					
					Arco nuevaConexion = new Arco(estacionIcicio, estacionFinal, costo);
					estacionIcicio.agregarArco(nuevaConexion);
					nombreEstacionActual = brRutas.readLine();
				}
				
				nombreRuta = brRutas.readLine();
			}
			else
			{
				brRutas.readLine();
				nombreRuta = brRutas.readLine();
			}
			
		}
		System.out.println("Se han cargado correctamente "+ grafo.darArcos().length+" arcos");
	}

	/**
	 * Busca el estacion con nombre dado<br>
	 * @param nombre el nombre del estacion buscado
	 * @return estacion cuyo nombre coincide con el parametro, null de lo contrario
	 */
	public Estacion buscarParadero(String nombre)
	{
		//TODO Implementar
		int idBuscar = 0;
		try
		{
			char letra = nombre.charAt(3);
			int numLetra = letra;
			idBuscar = Integer.parseInt(nombre.substring(0,3) + numLetra + nombre.substring(4,6));
		}
		catch(Exception e)
		{
			
		}
		
		Estacion respuesta = (Estacion) grafo.buscarNodo(idBuscar);
		return respuesta;
	}

}
