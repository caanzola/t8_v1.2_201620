package modelos;

public class Ruta 
{
	
	private String nombre;
	private int cantidadDeParadas;
	private Estacion[] paradas;
	private int dondeVa;

	public Ruta(String pNombre, int pCantidadParadas) 
	{
		// TODO Auto-generated constructor stub
		nombre = pNombre;
		cantidadDeParadas = pCantidadParadas;
		paradas = new Estacion[cantidadDeParadas];
		dondeVa = 0;
	}
	
	public void agregarEstacion(Estacion estacion)
	{
		paradas[dondeVa] = estacion;
		dondeVa++;
	}

	public Estacion[] darParadas() 
	{
		return paradas;
	}
	
	public String darNombre()
	{
		return nombre;
	}

}
