package modelos;

import estructuras.Arco;
import estructuras.Nodo;

/**
 * Clase que representa un paradero del sistema integrado de transporte SITP
 * @author SamuelSalazar
 */
//TODO La clase debe implementar la interface Nodo
public class Estacion<K extends Comparable<K>> implements Nodo<K>{

	/**
	 * Latitud del estación
	 */
	//TODO declare el atributo latitud
	private double latitud;
	
	/**
	 * Longitud del estación
	 */
	//TODO Declare el atributo longitud	
	private double longitud;
	
	/**
	 * Nombre del paradero
	 */
	//TODO declare el nombre del paradero
	private String nombre;
	
	/**
	 * Identificador único del paradero
	 */
	//TODO Declare el identificador del paradero
	private K id;
	
	private Arco[] conexiones;
	
	private int dondeVa;
	
	
	/**
	 * Construye un nuevo paradero con un nombre e id dado en una ubicacion
	 * representada por la latitud y la longitud.
	 * @param id Indentificador unico del paradero
	 * @param latitud 
	 * @param longitud 
	 * @param nombre
	 */
	public Estacion(K pId, double platitud, double plongitud, String pnombre) 
	{
		//TODO implementar
		id = pId;
		latitud = platitud;
		longitud = plongitud;
		nombre = pnombre;
	}
	
	public K darId(){
		//TODO implementar
		return id;
	}
	
	/**
	 * Devuelve la latitud del paradero
	 * @return latitud
	 */
	public double darLatitud() {
		//TODO implementar
		return latitud;
	}
	
	/**
	 * Devuelve la longitud del paradero
	 * @return longitud
	 */
	public double darLongitud() {
		//TODO implementar
		return longitud;
	}
	
	/**
	 * Devuelve el nombre del paradero
	 * @return nombre
	 */
	public String darNombre() {
		//TODO implementar
		return nombre;
	}
	
	/**
	 * nombre (latitud, longitud)
	 */
	@Override
	public String toString() {
		return nombre+" ("+latitud+" , "+longitud+")";
	}

	@Override
	public int compareTo(Nodo o) 
	{
		return (Integer)o.darId() - (Integer)id;
	}
	
	public void inicializarArregloDeConexiones(int tamanio)
	{
		conexiones = new Arco[tamanio];
		dondeVa = 0;
	}
	
	public void agregarArco(Arco nuevaConexion)
	{
		conexiones[dondeVa] = nuevaConexion;
		dondeVa++;
	}
	
	public Arco[] darAdyacentes()
	{
		return conexiones;
	}

}
